#!/usr/bin/env bash

# Check for Distro (live or sid)
# items needed: Webserver | Mysql server | 
if ! source install.conf; then
    echo ""
    read -p "Please enter Distro: " distro
    read -p "please enter Mysql Server: " mysqlServer
    read -p "Pease enter Web Server: " webServer
    
    printf "distro=${distro}\nmysqlServer=${mysqlServer}\nwebServer=${webServer}\n" > "install.conf"
fi

source install.conf

    if [ ! -z $webServer ]; then

        if [ $webServer == "apache2" ]; then
            PKGS+=( 'apache2' )
        elif [ $webServer == "nginx" ]; then
            PKGS+=( 'nginx' )
        else
            printf "no webserver picked\n"
        fi
    fi

    if [ ! -z $mysqlServer ]; then
        
        if [ $mysqlServer == "mariadb" ]; then
        PKGS+=( 'mariadb' )
        elif [ $mysqlServer == "mysql" ]; then
        PKGS+=( 'mysql' )
        fi

    fi


echo ${PKGS[@]}