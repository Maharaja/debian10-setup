#!/bin/bash

if [ "$EUID" != 0 ]; then
    echo "Run As Root"
    exit 1
fi

PKGS=(
    'kmag'
    'libreoffice*'
    'xterm'
    'xiterm+thai'
    'mlterm'
    'kate'
    'dragonplayer'
    'juk'
    'sweeper'
    'okular'
    'konqueror'
    'goldendict'
    'gwenview'
    'imagemagick'
    'firefox-esr'
    'k3b'
    'kwalletmanager'
    'konsole'
    'kcalc'
    'kmouth'
    'knotes'
    'kwrite'
    'kmail'
    'kaddressbook'
    'korganizer'
    'akregator'
    'kopete'
    'plasma-discover' # Discover app for debian (app install)
)

sudo apt-get purge -y ${PKGS[@]} && sudo apt autoremove -y

echo -e "\nDone Removing Packeges!\n"