#!/bin/bash

if [ "$EUID" != 0 ]; then
    echo "Run As Root"
    exit 1
fi

PKGS=(
    'openssh-server' # Remote connection to debian via SSH
    'git' # For getting projects of github\gitlab
    'wget' # For downloading stuff
    'curl' # For downloading stuff
    'xrdp' # Remote Desktop Support
    'tigervnc-standalone-server' # Remote Desktop Support
    'terminator' # new Terminal that i like to use
)

sudo apt update && sudo apt-get install -y ${PKGS[@]}

sudo -u $SUDO_USER -H sh -c "git clone https://gitlab.com/Maharaja/debian10-setup.git setup" 

cd setup

sudo chmod +x "01-removeSoftware.sh"
sudo chmod +x "02-installSoftware.sh"

echo -e "\nDone Pre-Installing!\n" 

# When script is done delete its self
rm ../00-preinstall.sh
exit 0