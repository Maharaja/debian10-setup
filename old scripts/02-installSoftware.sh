#!/bin/bash

if [ "$EUID" != 0 ]; then
    echo "Run As Root"
    exit 1
fi

# software to add: webserver (apache | Nginx) - mariadb-server|client - phpmyadmin - composer - 
# clang make libmariadbclient-dev libssl-dev libbz2-dev libreadline-dev libncurses-dev libboost-all-dev mariadb-server p7zip default-libmysqlclient-dev
 
PKGS=(
    'openjdk-8-jdk' # Java Install
    'code' # VSCode
    'gimp' # Gimp | Create images and edit photographs
    'cmake' # For compiling Project
    'make' 
    'mariadb-server' #Mysql Database
    'g++' 
    'gcc'
)

sudo apt-get install -y ${PKGS[@]}

sudo update-alternatives --config java

echo -e "\nDone!\n"