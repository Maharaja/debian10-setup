#!/bin/bash

#Do not edit under here

if [ "$EUID" != 0 ]; then
    echo "Please run as normaal user (root user will be use when need to)"
    exit 1
fi

if ! test -f package.conf; then
    echo "No package.conf found please read the package.example.conf file for unstructions"
    exit 1
else
    source package.conf
fi

# SETUP OF THE SCRIPT
if ! test -f install.conf; then
    clear
    echo ""
    printf "Distro Options:\n live: Default\n sid\n"
    read -p "Please enter Distro: " distro
    distro=${distro:-live}
    clear

    printf "Mysql Options:\n 1. Mariadb (default)\n 2. mysql\n"
    read -p "please enter Mysql Server: " mysqlServer
    mysqlServer=${mysqlServer:-1}
    clear

    printf "WebServer Options:\n 1. apache2 (default)\n 2. nginx\n"
    read -p "Pease enter Web Server: " webServer
    webServer=${webServer:-1}
    clear
    
    printf "Jetbrains toolbox Options:\n Yes\n No (default)\n"
    read -p "Want To Install Jetbrains Toolbox: " jetbrains
    jetbrains=${jetbrains:-no}
    clear
    
    printf "Youtube-DL Options:\n Yes\n No (default)\n"
    read -p "Want to install Youtube-DL (Yes|No): " YoutubeDL
    YoutubeDL=${YoutubeDL:-no}
    clear

    printf "VSCode Options:\n Yes\n No (default)\n"
    read -p "Want to install Youtube-DL (Yes|No): " VSCode
    YoutubeDL=${VSCode:-no}
    clear
    
    printf "Browser Options\n1. chrome\n2. firefox\n3. firefox-esr\n"
    read -p "What Browser do you want to use: " Browser
    Browser=${Browser:-1}
    clear

    printf "distro=${distro}\nmysqlServer=${mysqlServer}\nwebServer=${webServer}\njetbrains=${jetbrains}\nyoutubeDL=${YoutubeDL}\nbrowser=${Browser}\nvscode=${VSCode}" > "install.conf"
else
    source install.conf
fi

cd "$(dirname "$0")"

# Use REMOVE_PKGS Array to remove the software and autoremove all dependecies as well
sudo apt-get purge -y ${REMOVE_PKGS[@]} && sudo apt autoremove -y
echo "Removing unwanted apps" >> log.log

# Change the distro if needed
if [ $distro == "sid" ]; then
echo "Changing to Sid distro" >> log.log

sudo cat <<EOF > /etc/apt/sources.list
# Changed by debian10-setup by Maharaja on $(date)
deb http://deb.debian.org/debian/ sid main non-free contrib
deb-src http://deb.debian.org/debian/ sid main non-free contrib
EOF

# Update system after change to sid
sudo apt-get update && sudo apt full-upgrade -y

fi

# Add Chrome if its 1 Files
if [ $browser == '1' ]; then
cd "$(dirname "$0")"
echo "installing Chrome" >> log.log

wget -P https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb 
sudo apt install ./temp/google-chrome-stable_current_amd64.deb

fi

# Adding VSCode To the sources folder (need to check if thet need the key in: /etc/apt/trusted.gpg.d/ 'microsoft.gpg')
if [ ${vscode,,} == 'yes' ]; then
cd "$(dirname "$0")"
echo "installing VSCode" >> log.log

curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > temp/microsoft.gpg
sudo install -o root -g root -m 644 temp/microsoft.gpg /usr/share/keyrings/microsoft-archive-keyring.gpg

sudo cat <<EOF > /etc/apt/sources.list.d/vscode.list
### THIS FILE IS AUTOMATICALLY CONFIGURED ###
# You may comment out this entry, but any other modifications may be lost.
deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-archive-keyring.gpg] https://packages.microsoft.com/repos/vscode stable main

EOF

fi

# Select The Webserver To Install
case $webServer in
    "1")
        echo "Adding apache2 to INSTALL_PKGS" >> log.log
        INSTALL_PKGS+=( 'apache2' )
    ;;
    "2")
        echo "Adding nginx to INSTALL_PKGS" >> log.log
        INSTALL_PKGS+=( 'nginx' )
    ;;
    *)
esac

# Select The Mysql Server To Install
case $mysqlServer in
    "1")
        echo "Adding mariadb-server to INSTALL_PKGS" >> log.log
        INSTALL_PKGS+=( 'mariadb-server' )
    ;;
    "2")
        echo "Adding mysql-server to INSTALL_PKGS" >> log.log
        INSTALL_PKGS+=( 'mysql-server' )
    ;;
    *)
esac

#TODO Check if i can add chrome with a source list add and then a update + INSTALL_PKGS Array Add
# Check what browser you want to install (need to extent with more options and change the selection later on to numbers of its to mutch)
# 1. chrome | 2. firefox | 3. firefox-esr
case ${browser,,} in
    "2")
        echo "Adding firefox to INSTALL_PKGS" >> log.log
        INSTALL_PKGS+=( 'firefox' )
    ;;
    "3")
        echo "Adding firefox-esr to INSTALL_PKGS" >> log.log
        INSTALL_PKGS+=( 'firefox-esr' )
    ;;
    *)
esac

echo -e "\nDone Removing Packeges!\n"

# Update Sourcelist and install new packages
sudo apt update && sudo apt-get install -y ${INSTALL_PKGS[@]}

# Install Youtube-DL
if [ ${youtubeDL,,} == 'yes' ]; then
    echo "Installing Youtube-DL" >> log.log
    sudo apt install python
    sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
    sudo chmod a+rx /usr/local/bin/youtube-dl
fi

# Fixes Install and Update and upgrade the compleet system
echo "Fixing Install and update" >> log.log
sudo apt install -f && sudo apt update && sudo apt upgrade

# Change the java version if needed
sudo update-alternatives --config java

#clearing
echo "Removing Temp file" >> log.log
sudo rm -rf temp

echo -e "\nDone installing Debian!\n"
