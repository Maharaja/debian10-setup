Debian10 Setup Script
==========================

### Requirements
- Debian 10.6.0


## Install Info
Pre setup install info
```shell
sudo apt install openssh-server git
git clone https://gitlab.com/Maharaja/debian10-setup.git setup
cd setup
sudo chmod +x "debian10-setup.sh"
sudo ./debian10-setup.sh
```

Follow instructions inside debian10-setup.sh

## Common Installation Issues
None as of now